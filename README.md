## SAMPLE REACT NGINX

## Docker
### Environment Variables
Environment variables are resolved in *.template files in nginx/templates folder.

NGINX_HOST: The hostname 
NGINX_PORT: The port for nginx to listen

```bash
docker run -d --name iclchan-ui -p3002:80 -e NGINX_HOST=localhost -e NGINX_PORT=80 iclchan-ui:0.0.1
```