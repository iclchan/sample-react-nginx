FROM node:18.12.1 as builder

COPY . .

RUN npm ci --only=production
RUN npm run build

FROM nginx:1.23.3
ARG nginx_server_name=localhost
ENV NGINX_HOST=$nginx_server_name

COPY --from=builder build /usr/share/nginx/html
COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY nginx/templates /etc/nginx/templates

# Expose the port for access
EXPOSE 80

# Run the nginx server
CMD ["nginx", "-g", "daemon off;"]
